-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2019 at 01:36 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `socialapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vote` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`, `vote`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 0, '2019-11-03 04:30:40', '2019-11-03 22:33:59'),
(2, 3, 2, 1, '2019-11-03 04:38:36', '2019-11-03 22:27:20'),
(3, 4, 2, 0, '2019-11-03 04:58:36', '2019-11-03 22:26:53'),
(4, 7, 2, 1, '2019-11-03 12:29:26', '2019-11-03 22:33:55'),
(5, 6, 2, 1, '2019-11-03 12:29:34', '2019-11-03 22:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_11_02_060938_create_users_table', 1),
(2, '2019_11_02_121646_create_posts_table', 2),
(3, '2019_11_03_033154_create_likes_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `body`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 'Fantastic job.  Perfect pacing for following along to recreate what you are doing.  Great explanations.  Thank you for posting this!', 4, '2019-11-02 19:00:38', '2019-11-02 19:00:38'),
(4, 'Hi first i\'d like to thank you for the great job you are doing in here Thank you so much\r\ni\'ve a question about the laravel framework or frameworks in general now im at the video #11 and i\'ve understood almost all the features that you were talking about.', 1, '2019-11-02 19:00:58', '2019-11-02 19:00:58'),
(5, 'Hi,I have watched all this playlist videos of creating a social network with Laravel!\r\nI am really enjoying this tutorials, and I am codding this project at the same time.\r\nKeep with the good work', 2, '2019-11-02 19:04:47', '2019-11-02 19:04:47'),
(6, 'Ambitioni dedisse scripsisse iudicaretur. Cras mattis iudicium purus sit amet fermentum. Donec sed odio operae, eu vulputate felis rhoncus.', 2, '2019-11-03 06:09:27', '2019-11-03 23:30:54'),
(7, 'At nos hinc posthac, sitientis piros Afros. Petierunt uti sibi concilium totius Galliae in diem certam indicere.', 2, '2019-11-03 06:21:31', '2019-11-03 06:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `created_at`, `updated_at`) VALUES
(1, 'alex@exp.com', 'Abu Naseer', '$2y$10$YIi.yvCSqIah4D6I5rUP0.PBqU8kJrch1/kMnilDwOILcCzRVGrWi', '2019-11-02 07:54:39', '2019-11-02 07:54:41'),
(2, 'naseer@exp.com', 'Neesam', '$2y$10$w5cCcJyOf/FjHWlYeb1Fsufn4EV/JCEy2ObAGp13W2WIpfVmqBeVa', '2019-11-03 00:18:45', '2019-11-03 00:18:45'),
(3, 'sshezan@exp.com', 'Shezan', '$2y$10$q1YmM4mQVAk/DXVELUlZpuUtsG2vPaxJTW4dc37xzK4RtTsqrJXdy', '2019-11-02 08:29:42', '2019-11-02 08:29:41'),
(4, 'fansee@gmail.com', 'Fans', '$2y$10$aibKGwYD4Nnd0CskGT6VAOw48i9y6EZR6jRWqajcn7pUxh9YEdvqy', '2019-11-03 00:19:31', '2019-11-03 00:19:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
