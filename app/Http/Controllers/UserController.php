<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Session;
use Auth;
use DB;

class UserController extends Controller
{
    public function postSignIn(Request $request)
    {
    	$crediential = [
    		'email' => $request->email,
    		'password' => $request->password
    	];

    	if (Auth::guard('admin')->attempt($crediential)) {
    		Session::put('admin', Auth::guard('admin')->user());
            $adminId = Auth::guard('admin')->user()->id;
            Session::put('admin_id',$adminId);
            return redirect()->route('dashboard');
    	}else{
    		return redirect()->back()->withErrors(['message' => 'Login Unsuccessful!']);
    	}
    	// return $request->all();
    }

    public function logout()
    {
        // return 'hi';
    	if (Session::has('admin')) {
    		// Session::forget(Auth::guard('admin')->user());
    		auth('admin')->logout();
    		// Session::flash();
    		return redirect(url('/'));
    	}
    }

    public function postSignUp(Request $request)
    {
    	$this->validate($request, [
    		'email' 	=> 'required|email',
    		'name' 		=>'required',
    		'password' 	=> 'required|min:4'
    	]);
		try {
			DB::beginTransaction();
    		$inserted = User::insert([
    			'name' 		=> $request->name,
    			'email'		=> $request->email,
    			'password'	=> bcrypt($request->password)
    		]);

    		if ($inserted) {
    			DB::commit();
    			return Redirect()->back()->with(['message' => 'SignIn Successful']);
    		}else{
    			DB::rollBack();
    			return Redirect()->back()->withErrors(['message' => 'Something ware wrong!']);
    		}
    		
    	} catch (Exception $e) {
    		DB::rollBack();
    		return Redirect()->back()->withErrors(['message' => $e->errorInfo[2]]);
    	}
    }
}
