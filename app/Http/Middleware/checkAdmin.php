<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;

class checkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->user()) {
            if (Session::has('admin')) {
                return $next($request);
            }
        } 
        return redirect('/');
    }
}
