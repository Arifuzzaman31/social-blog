<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'user_name' => $faker->name,
        'user_email' => $faker->unique()->safeEmail,
    ];
});

$factory->define(App\Course::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomDigit,
        'teacher_id' => $faker->randomDigit,
        'course_name' => $faker->word,
    ];
});

$factory->define(App\Teacher::class, function (Faker $faker) {
    return [
        'course_id' => $faker->randomDigit,
        'teacher_name' => $faker->name,
        'phone' => $faker->phoneNumber,
    ];
});